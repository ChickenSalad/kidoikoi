<!DOCTYPE html>
<html>
    <head>
        <title> Kidoikoi </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">  <!-- Mobile First -->

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>

    <body>

        <!-- La navbar se réduit quand la fenêtre atteint SM, background dark, navbar-dark pour afficher le logo collapse et enlever les bleus href  -->
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">

            <!-- Logo -->
            <a class="navbar-brand" href="index.php"> Kidoikoi </a>

            <!-- Bouton collapse -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapseNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Liens --> 
            <div class="collapse navbar-collapse" id="collapseNavbar">   
                <ul class="navbar-nav ml-auto">     <!-- navbar-nav sert à mettre en forme et enlever les points de ul, ml-auto pour les positionner vers la droite -->  
                    <li class="nav-item">
                        <a class="nav-link" href="#"> Se connecter </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> S'inscrire </a>
                    </li>
                </ul>
            </div>

        </nav>

        <!-- Bootstrap --> 
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>